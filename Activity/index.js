let trainer = {
    name: "Ash",
    age: 24,
    pokemon: ["Pikachu", "Geodude", "Mewtwo", "Dugtrio"],
    friends: {
        firstName: ["Misty", "Brock"]
    },
    talk: function(){
        console.log("Pikachu, I choose you!");
    }
}


function Pokemon(name, level) {

      // Properties
      this.name = name;
      this.level = level;
      this.health = 2 * level;
      this.attack = level;

      //Methods
      this.tackle = function(target) {
          console.log(this.name + ' tackled ' + target.name);
          target.health -= this.attack;
          console.log( target.name + "'s health is now reduced to " + target.health);
          if(target.health <=0){
            this.faint(target.name);
          }
          };
      this.faint = function(target){
          console.log(target + ' has fainted.');
      }

  }

// Creates new instances of the "Pokemon" object each with their unique properties

console.log(trainer);
console.log("Trainer name: "+trainer.name);
trainer.talk()
let pikachu = new Pokemon("Pikachu", 5);
let rattata = new Pokemon('Rattata', 8);
let diglet = new Pokemon('Diglet', 1);

console.log(pikachu);
console.log(rattata);
console.log(diglet);

  // Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the two objects
pikachu.tackle(diglet);
rattata.tackle(pikachu);


